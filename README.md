# Anki2PaperGenerator

I extensively use Anki for spaced repetition learning. Sometimes, it can be interesting to have the paper version as well. I have developed a 3D-printed board especially for that, which you can find [here](https://gitlab.com/jytou/3d-paper-spaced-repetition), and this project enables to paper print Anki decks. For more info, read this article on my blog: https://blog.jytou.fr/2019/11/13/a-board-for-paper-based-spaced-repetition-memorization/

# Analyze your Anki Deck

So far, you will need to write a little Java to get going. Just have a look at src/jyt/anki/card/generator/AnkiCardExtractor classes and write an extractor to build an Anki card with a front side and a back side from the fields of your Anki cards.

Then you can run AnkiPhysicalCardGenerator to generate a PDF with all the cards of your deck! Et voilà!
