package jyt.anki.card.generator;

public interface IAnkiCardExtractor
{
	AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId);
}
