package jyt.anki.card.generator;

import java.util.List;

public interface ICardCollector
{
	void collect(AnkiCard pCard);
	List<AnkiCard> getCollectedCards();
}
