package jyt.anki.card.generator;

public class AnkiCardExtractorMorseCode implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		if ((pOrder != 0) || (pFields.length != 4))
			return new AnkiCard[0];
		final String letter = pFields[0];
		if ((letter == null) || letter.isEmpty())
			return new AnkiCard[0];
		String mem = pFields[1];
		String morse = pFields[3];
		if (mem.replace("0", "").replace("1", "").isEmpty())
		{
			// we have a morse code in the mem and the mem is the morse
			final String tamp = mem.replace("0", "-").replace("1", ".");
			mem = morse;
			morse = tamp;
		}
		return new AnkiCard[]
		{
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, 0, letter + " / Morse Mem?", mem + " / " + morse, "", false),
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, 0, letter + " / Morse?", morse + " / " + mem, "", false),
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, 0, morse + " / Morse Character?", letter + " / " + mem, "", false),
		};
	}

	private void appendIfFilled(StringBuilder pContainer, String pType, String pNew)
	{
		if ((pNew != null) && (pNew.trim().length() > 0))
		{
			if (pContainer.length() > 0)
				pContainer.append(", ");
			if ("/".equals(pType))
				pContainer.append("/").append(pNew);
			else
				pContainer.append(pType).append(": ").append(pNew);
		}
	}
}
