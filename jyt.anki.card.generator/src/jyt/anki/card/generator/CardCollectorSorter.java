package jyt.anki.card.generator;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CardCollectorSorter implements ICardCollector
{
	private ICardCollector mCardCollector;

	public CardCollectorSorter(ICardCollector pCardCollector)
	{
		super();
		mCardCollector = pCardCollector;
	}

	@Override
	public void collect(AnkiCard pCard)
	{
		mCardCollector.collect(pCard);
	}

	@Override
	public List<AnkiCard> getCollectedCards()
	{
		final List<AnkiCard> l = new ArrayList<>(mCardCollector.getCollectedCards());
		l.sort(new Comparator<AnkiCard>()
		{
			@Override
			public int compare(AnkiCard a1, AnkiCard a2)
			{
				if (a1.getNextLearn() == 0)
				{
					if (a2.getNextLearn() == 0)
					{
						// sort using the original order since both cards have not been seen yet
						final int noteid = new Long(a1.getNoteId()).compareTo(a2.getNoteId());
						if (noteid == 0)
							return new Long(a1.getOrder()).compareTo(a2.getOrder());
						else
							return noteid;
					}
					else
					{
						// a2 has been seen, but not a1
						return 1;
					}
				}
				else
				{
					if (a2.getNextLearn() == 0)
					// a2 has not been seen but a1 has been
						return -1;
					else
					{
						int nextLearn = new Long(a1.getNextLearn()).compareTo(a2.getNextLearn());
						if (nextLearn != 0)
							return nextLearn;
						else
						{
							// sort using the original order since both cards have not been seen yet
							final int noteid = new Long(a1.getNoteId()).compareTo(a2.getNoteId());
							if (noteid == 0)
								return new Long(a1.getOrder()).compareTo(a2.getOrder());
							else
								return noteid;
						}
					}
				}
			}
		});
		return l;
	}
}
