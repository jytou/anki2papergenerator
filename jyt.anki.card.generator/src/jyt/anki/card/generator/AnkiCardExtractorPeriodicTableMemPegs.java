package jyt.anki.card.generator;

public class AnkiCardExtractorPeriodicTableMemPegs implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		final String image = pFields[0];
		final String elementName = pFields[1];
		final String atomicNumber = pFields[2];
		final String elementShort = pFields[3];
		final String mem = pFields[4];
		return new AnkiCard[]
		{
			// Name -> number and short
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, elementName + " (Periodic Table)", image + atomicNumber + " / " + elementShort, mem, false),
			// short -> name and number
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, elementShort + " (Periodic Table)", image + atomicNumber + " / " + elementName, mem, false),
			// number -> name and short
			new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, atomicNumber + " (Periodic Table)", image + elementName + " / " + elementShort, mem, false),
		};
	}
}
