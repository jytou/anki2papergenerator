package jyt.anki.card.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CardCollectorAll implements ICardCollector
{
	private List<AnkiCard> mCards = new ArrayList<>();
	private boolean mFrontOnly;

	public CardCollectorAll(boolean pFrontOnly)
	{
		super();
		mFrontOnly = pFrontOnly;
	}

	@Override
	public void collect(AnkiCard pCard)
	{
		if ((!mFrontOnly) || (pCard.getOrder() == 0))
			mCards.add(pCard);
	}

	@Override
	public List<AnkiCard> getCollectedCards()
	{
		List<AnkiCard> cards = new ArrayList<>(mCards);
		Collections.sort(cards, new Comparator<AnkiCard>()
		{
			@Override
			public int compare(AnkiCard pO1, AnkiCard pO2)
			{
				// TODO Auto-generated method stub
				return 0;
			}
		});
		return cards;
	}
}
