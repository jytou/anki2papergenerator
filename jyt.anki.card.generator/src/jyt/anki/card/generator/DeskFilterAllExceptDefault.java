package jyt.anki.card.generator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Consumer;

import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonObject.Member;

public class DeskFilterAllExceptDefault implements Consumer<JsonObject.Member>
{
	private SortedMap<String, Long> mDecksName2Id = new TreeMap<>();
	private Set<String> mNameFilter;

	public DeskFilterAllExceptDefault()
	{
		super();
	}

	public DeskFilterAllExceptDefault(String[] pNameFilter)
	{
		super();
		mNameFilter = new HashSet<>(Arrays.asList(pNameFilter));
	}

	@Override
	public void accept(Member m)
	{
		if (!"1".equals(m.getName()))
		{
			final JsonObject deckAsObject = m.getValue().asObject();
			final String name = deckAsObject.get("name").asString();
			if (mNameFilter == null)
				mDecksName2Id.put(name, deckAsObject.get("id").asLong());
			else
				for (String filter : mNameFilter)
					if (name.contains(filter))
					{
						mDecksName2Id.put(name, deckAsObject.get("id").asLong());
						break;
					}
		}
	}

	public SortedMap<String, Long> getDecksName2Id()
	{
		return mDecksName2Id;
	}
}