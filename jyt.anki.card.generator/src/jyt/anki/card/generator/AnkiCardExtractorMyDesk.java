package jyt.anki.card.generator;

public class AnkiCardExtractorMyDesk implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		final String front = pFields[pOrder == 0 ? 1 : 2];
		final String back = pFields[pOrder == 0 ? 2 : 1];
		final String mem = pFields[0];
		return new AnkiCard[] {new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, front, back, mem, true)};
	}
}
