package jyt.anki.card.generator;

public class AnkiCardExtractorFrenchDepartments implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		if (pFields.length == 6)
		{
			if (pOrder == 0)
			{
				final String mem = pFields[0];
				final String num = pFields[3];
				final String dpt = pFields[4];
				String chl = pFields[5];
				final String pref = "préfecture";
				// clean up the extra text Prefecture
				if (chl.contains(pref))
					chl = chl.substring(0, chl.indexOf(pref)) + chl.substring(chl.indexOf(pref) + pref.length());
				return new AnkiCard[]
				{
					new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, num + " / département", chl + " / " + dpt, mem, false),// num -> chl+dpt
					new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, chl, num + " / " + dpt, mem, false),// chl -> num+dpt
					new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, dpt, num + " / " + chl, mem, false)// dpt -> num+chl
				};
			}
			else
				return new AnkiCard[0];
		}
		else
		{
			/*if (pOrder == 0)
				
			final String front = pOrder == 0 ? pFields[0] : pFields[1];
			final String back = pOrder == 1 ? pFields[1] : pFields[0];
			final String mem = "";
			return new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, front, back, mem);*/
			return new AnkiCard[0];
		}
	}
}
