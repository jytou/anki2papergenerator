package jyt.anki.card.generator;

public class AnkiCardExtractorPeriodicTable implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		if (pFields.length == 5)
		// it is a normal element
		{
			final String fullName = pFields[0];
			final String mem = pFields[4];
			final String shortName = pFields[1];
			final String number = pFields[2];
			return new AnkiCard[]
			{
				new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, shortName + " (Tableau Périodique)", fullName + " (" + number + ")", mem, false),
				new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, fullName + " (Tableau Périodique)", shortName + " (" + number + ")", mem, false),
				new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, number + " (Tableau Périodique - Numéro atomique)", fullName + " / " + shortName, mem, false)
			};
		}
		else
		{
			final String question = pFields[0];
			final String mem = pFields[3];
			final String answer = pFields[1];
			// It is not a number, it's a line
			return new AnkiCard[]
			{
				new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, question, answer, mem, false)
			};
		}
	}
}
