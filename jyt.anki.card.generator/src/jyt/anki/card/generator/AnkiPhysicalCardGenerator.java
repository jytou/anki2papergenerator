package jyt.anki.card.generator;

import java.awt.Color;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType0Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonValue;

public class AnkiPhysicalCardGenerator
{
	private static final int PAGE_HEIGHT = 2970;
	private static final int PAGE_WIDTH = 2100;
	private static final float LINE_HEIGHT_FACTOR = 1.6f;
	private static final int FONT_SIZE = 40;
	private static final int CARD_HEIGHT = 190;
	private static final int CARD_WIDTH = 190;
	private static final int Y_ORIGIN = 100;
	private static final int X_ORIGIN = 160;
	private static final int START_BACK_X = PAGE_WIDTH - X_ORIGIN - CARD_WIDTH;
	private static final String CARD_QUERY = "select notes.flds, cards.type, cards.queue, cards.due, cards.ivl, cards.factor, cards.ord, notes.id from cards, notes where notes.id=cards.nid and cards.did=?";
	private static final int EXTRA_SPACE = 30;
	private static final float FORBID_SIZE = 20f;
	private static final String IMAGE_TAG = "[[IMAGE]]";
	private static final float IMAGE_HEIGHT = 50f;

	public static void main(String[] args) throws SQLException, ClassNotFoundException, IOException
	{
		new AnkiPhysicalCardGenerator(args[0], args[1]).generatePDF();
	}

	private String mOutFileName;
	private String mDbFileName;

	public AnkiPhysicalCardGenerator(final String dbFileName, final String outFileName)
	{
		super();
		mDbFileName = dbFileName;
		mOutFileName = outFileName;
	}

	public void generatePDF() throws ClassNotFoundException, SQLException, IOException
	{
		ICardCollector cardCollector = new CardCollectorSorter(new CardCollectorAll(true));
//		final String filter = "Deutsch: 5000 words";
//		final String filter = "000 Test";
//		final String filter = "TokiPona";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorTokiPona();
//		final String filter = "Morse Code";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorMorseCode();
//		final String filter = "Table Elements";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorPeriodicTable();
//		final String filter = "memory pegs";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorPeriodicTableMemPegs();
		final String filter = "My Deck";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorMyDesk();
//		final String filter = "Départements français";final IAnkiCardExtractor cardExtractor = new AnkiCardExtractorFrenchDepartments();
		final DeskFilterAllExceptDefault deskfilter = new DeskFilterAllExceptDefault(new String[] {filter});

		extractCards(cardCollector, cardExtractor, deskfilter);

		final PDDocument document = new PDDocument();
		PDPage currentPage = new PDPage(new PDRectangle(PAGE_WIDTH, PAGE_HEIGHT));
		PDPageContentStream content = new PDPageContentStream(document, currentPage, PDPageContentStream.AppendMode.APPEND, true);
		PDType0Font font = PDType0Font.load(document, AnkiPhysicalCardGenerator.class.getResourceAsStream("/LiberationSerif-Regular.ttf"), true);
		int x = X_ORIGIN;
		int y = Y_ORIGIN;
		List<AnkiCard> frontCards = new ArrayList<>();
		for (AnkiCard ankiCard : cardCollector.getCollectedCards())
		{
			if (x == X_ORIGIN)
				showText(content, font, 50, y + CARD_HEIGHT / 2, String.valueOf(ankiCard.getNextLearn()), 40);
			showCard(document, content, font, x, y, ankiCard.getFront(), false);
			frontCards.add(ankiCard);
			x += CARD_WIDTH;
			if (x >= PAGE_WIDTH - CARD_WIDTH * 1.5)
			{
				y += CARD_HEIGHT;
				x = X_ORIGIN;
				if (y >= PAGE_HEIGHT - CARD_HEIGHT * 1.5)
				{
					content.close();
					document.addPage(currentPage);
					currentPage = new PDPage(new PDRectangle(PAGE_WIDTH, PAGE_HEIGHT));
					content = new PDPageContentStream(document, currentPage, PDPageContentStream.AppendMode.APPEND, true);
					y = Y_ORIGIN;
					showBackCards(document, content, font, frontCards);
					content.close();
					document.addPage(currentPage);
					currentPage = new PDPage(new PDRectangle(PAGE_WIDTH, PAGE_HEIGHT));
					content = new PDPageContentStream(document, currentPage, PDPageContentStream.AppendMode.APPEND, true);
					frontCards.clear();
				}
			}
		}
		if (!frontCards.isEmpty())
		{
			content.close();
			document.addPage(currentPage);
			currentPage = new PDPage(new PDRectangle(PAGE_WIDTH, PAGE_HEIGHT));
			content = new PDPageContentStream(document, currentPage, PDPageContentStream.AppendMode.APPEND, true);
			showBackCards(document, content, font, frontCards);
		}
		content.close();
		document.addPage(currentPage);
		document.save(mOutFileName);
		document.close();
	}

	public void showBackCards(PDDocument pDocument, PDPageContentStream pContent, PDType0Font font, List<AnkiCard> pCards) throws IOException
	{
		AnkiCard card = null;
		final Iterator<AnkiCard> it = pCards.iterator();
		for (int backX = START_BACK_X, backY = Y_ORIGIN; backY < PAGE_HEIGHT - CARD_HEIGHT * 1.5;)
		{
			if (it.hasNext())
				card = it.next();
			else
				card = null;
			showCard(pDocument, pContent, font, backX, backY, card == null ? " " : card.getBack() + " / " + card.getMem(), true);
			if ((card != null ) && (!card.isDoubleSided()))
			{
				final PDImageXObject img = PDImageXObject.createFromFile(AnkiPhysicalCardGenerator.class.getResource("/forbid.png").getPath(), pDocument);
				pContent.drawImage(img, backX + CARD_WIDTH - FORBID_SIZE - 2, backY + CARD_HEIGHT - FORBID_SIZE - 2, FORBID_SIZE, FORBID_SIZE);
				pContent.stroke();
			}
			backX -= CARD_WIDTH;
			if (backX <= CARD_WIDTH * 0.5)
			{
				backY += CARD_HEIGHT;
				backX = START_BACK_X;
			}
		}
	}

	public void showCard(PDDocument pDocument, PDPageContentStream pContent, PDType0Font pFont, int x, int y, final String pCardTxt, final boolean pShowRect) throws IOException
	{
		if (pShowRect)
		{
			pContent.setLineWidth(1f);
			pContent.setNonStrokingColor(Color.black);
			pContent.addRect(x, y, CARD_WIDTH, CARD_HEIGHT);
			pContent.stroke();
		}
		final String txt = cleanHTML(pCardTxt);
		int fs = computeOptimalFontSize(pFont, txt);
		showCardText(pDocument, pContent, pFont, x, y, txt, fs);
	}

	public void extractCards(ICardCollector pCardCollector, final IAnkiCardExtractor pCardExtractor, final DeskFilterAllExceptDefault pDeskfilter) throws ClassNotFoundException, SQLException
	{
		Class.forName("org.sqlite.SQLiteConnection");
		Connection conn = null;
		try
		{
			conn = DriverManager.getConnection("jdbc:sqlite:" + mDbFileName);
			final Statement s = conn.createStatement();
			ResultSet q = s.executeQuery("select decks from col");

			while (q.next())
			{
				final String decksJsonString = q.getString(1);
				JsonValue decksJson = Json.parse(decksJsonString);
				decksJson.asObject().forEach(pDeskfilter);
				for (String name : pDeskfilter.getDecksName2Id().keySet())
					System.out.println(name + " -> " + pDeskfilter.getDecksName2Id().get(name));
			}
			q.close();
			final PreparedStatement ps = conn.prepareStatement(CARD_QUERY);
			for (String name : pDeskfilter.getDecksName2Id().keySet())
			{
				final Long did = pDeskfilter.getDecksName2Id().get(name);
				ps.setLong(1, did);
				q = ps.executeQuery();
				while (q.next())
				{
					final String fields = q.getString(1);
					final String[] fs = fields.split("", -1);
					final int type = q.getInt(2);
					final int queue = q.getInt(3);
					final int due = q.getInt(4);
					final int ivl = q.getInt(5);
					final int factor = q.getInt(6);
					final int ord = q.getInt(7);
					final int id = q.getInt(8);
					final AnkiCard[] cards = pCardExtractor.extractCards(fs, ivl, queue < 0 ? -1 : ivl, ord, id);
					for (AnkiCard card : cards)
					{
						pCardCollector.collect(card);
						System.out.println("front: " + card.getFront() + ", back: " + card.getBack() + ", days: " + card.getNextLearn() + ", ord: " + card.getOrder());
						System.out.println("fields (" + fs.length + "): " + fields + ", type=" + type + ", queue: " + queue + ", due: " + due + ", ivl: " + ivl + ", factor: " + factor + ", ord: " + ord);
					}
				}
				q.close();
			}
			ps.close();
			s.close();
		}
		finally
		{
			if (conn != null)
				conn.close();
		}
	}

	public static int computeOptimalFontSize(PDType0Font font, final String txt) throws IOException
	{
		int fs = FONT_SIZE;
		while (computeHeight(font, txt, fs) < CARD_HEIGHT * 0.8)
			fs++;
		fs--;
		while (computeHeight(font, txt, fs) > CARD_HEIGHT * 0.8)
			fs--;
		if (fs > 40)
			fs = 40;
		return fs;
	}

	private static String cleanHTML(String pTxt)
	{
		int index;
		// Deleting all html tags
		while ((index = pTxt.indexOf('<')) != -1)
		{
			final String startTag = pTxt.substring(pTxt.indexOf('<') + 1).toLowerCase();
			String image = "";
			if (startTag.startsWith("img"))
				image = startTag.substring(startTag.indexOf("src=\"") + 5, startTag.substring(0, startTag.indexOf(">")).lastIndexOf('"')) + IMAGE_TAG;
			final String space = startTag.startsWith("div") | startTag.startsWith("br") ? " " : "";
			int end = pTxt.indexOf('>', index);
			if (end != -1)
				pTxt = image + (index > 0 ? pTxt.substring(0, index) : "") + space + (end < pTxt.length() - 1 ? pTxt.substring(end + 1) : "");
		}
		// Deleting sound tags
		while ((index = pTxt.indexOf("[sound:")) != -1)
		{
			int end = pTxt.indexOf(']', index);
			if (end != -1)
				pTxt = (index > 0 ? pTxt.substring(0, index) : "") + (end < pTxt.length() - 1 ? pTxt.substring(end + 1) : "");
		}
		// Transform html encoded characters into printable ones
		pTxt = StringEscapeUtils.unescapeHtml4(pTxt);
		int l = pTxt.length();
		// Make sure we don't have extra spaces
		do
		{
			pTxt = pTxt.replaceAll("  ", " ");
		}
		while (l > (l = pTxt.length()));
		return pTxt.trim();
	}

	private static void showCardText(PDDocument pDocument, PDPageContentStream pContent, PDType0Font pFont, int pX, int pY, String pText, int pFontSize) throws IOException
	{
		final float lineHeight = pFont.getFontDescriptor().getCapHeight() / 1000 * pFontSize;
		float imageHeight = 0;
		if (pText.contains(IMAGE_TAG))
		{
			final String image = pText.substring(0, pText.indexOf(IMAGE_TAG));
			imageHeight = IMAGE_HEIGHT;
			PDImageXObject img;
			if (image.startsWith("http"))
				img = PDImageXObject.createFromFile("/home/jytou/bignas/web" + image.substring(image.indexOf('/', 9)), pDocument);
			else
				img = PDImageXObject.createFromFile("/home/jytou/.local/share/Anki2/User 1/collection.media/" + image, pDocument);
			float imgWidth = img.getWidth() * IMAGE_HEIGHT / img.getHeight();
			float imgHeight = IMAGE_HEIGHT;
			if (imgWidth >= CARD_WIDTH)
			{
				// the image is too large, reduce its size
				imgHeight = IMAGE_HEIGHT * CARD_WIDTH / img.getWidth();
				imgWidth = CARD_WIDTH;
			}
			pContent.drawImage(img, pX + (CARD_WIDTH - imgWidth) / 2, pY + CARD_HEIGHT - imgHeight - FORBID_SIZE / 3, imgWidth, imgHeight);
			pContent.stroke();
			pText = pText.substring(pText.indexOf(IMAGE_TAG) + IMAGE_TAG.length());
		}
		final StringTokenizer st = new StringTokenizer(pText, " -", true);
		String line = "";
		List<String> lines = new ArrayList<>();
		int x = 0;
		while (st.hasMoreTokens())
		{
			String t = st.nextToken();
			float ww = computeWordWidth(pFont, pFontSize, t);
			if (x + ww > CARD_WIDTH - EXTRA_SPACE)
			{
				x = 0;
				lines.add(line);
				line = "";
			}
			x += ww;
			line += t;
			if (st.hasMoreTokens())
			{
				// space
				ww = computeWordWidth(pFont, pFontSize, t = st.nextToken());
				if (x + ww > CARD_WIDTH - EXTRA_SPACE)
				{
					x = 0;
					lines.add(line);
					line = "";
				}
				else
				{
					x += ww;
					line += t;
				}
			}
		}
		if (!line.isEmpty())
			lines.add(line);
		float y = (CARD_HEIGHT - imageHeight) / 2 + (lines.size() - 1) * lineHeight * LINE_HEIGHT_FACTOR / 2;
		for (String l : lines)
		{
			showText(pContent, pFont, pX + CARD_WIDTH / 2 - computeWordWidth(pFont, pFontSize, l.trim()) / 2, pY + y - lineHeight / 2, l.trim(), pFontSize);
			y -= lineHeight * LINE_HEIGHT_FACTOR;
		}
	}

	public static float computeWordWidth(final PDFont pFont, final float pFontSize, final String pWord) throws IOException
	{
		if (" ".equals(pWord))
			return pFont.getSpaceWidth() / 1000 * pFontSize;
		else
			return pFont.getStringWidth(pWord) / 1000 * pFontSize;
	}

	public static float computeHeight(PDFont pFont, String pText, float pFontSize) throws IOException
	{
		final float lineHeight = pFont.getFontDescriptor().getCapHeight() / 1000 * pFontSize;
		float h = lineHeight;
		if (pText.contains(IMAGE_TAG))
		{
			h += IMAGE_HEIGHT;
			pText = pText.substring(pText.indexOf(IMAGE_TAG) + IMAGE_TAG.length());
		}
		StringTokenizer st = new StringTokenizer(pText, " -", true);
		int x = 0;
		while (st.hasMoreTokens())
		{
			String t = st.nextToken();
			float ww = computeWordWidth(pFont, pFontSize, t);
			if (ww > CARD_WIDTH - EXTRA_SPACE)
				return 100000;
			if (x + ww > CARD_WIDTH - EXTRA_SPACE)
			{
				x = 0;
				h += lineHeight * LINE_HEIGHT_FACTOR;
			}
			x += ww;
			if (st.hasMoreTokens())
			{
				// space
				ww = computeWordWidth(pFont, pFontSize, st.nextToken());
				if (x + ww > CARD_WIDTH - EXTRA_SPACE)
				{
					x = 0;
					h += lineHeight * LINE_HEIGHT_FACTOR;
				}
				else
					x += ww;
			}
		}
		return h;
	}

	@SuppressWarnings("deprecation")
	public static void showText(PDPageContentStream pContent, PDFont pFont, final float xPos, final float yPos, final String pText, float pFontSize) throws IOException
	{
		pContent.beginText();
		pContent.setFont(pFont, pFontSize);
		pContent.moveTextPositionByAmount(xPos, yPos);
		pContent.showText(pText);
		pContent.endText();
	}
}
