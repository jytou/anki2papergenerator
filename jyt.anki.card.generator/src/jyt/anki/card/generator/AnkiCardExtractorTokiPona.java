package jyt.anki.card.generator;

public class AnkiCardExtractorTokiPona implements IAnkiCardExtractor
{
	@Override
	public AnkiCard[] extractCards(String[] pFields, long pNextLearn, long pNbSuccess, long pOrder, long pNoteId)
	{
		final String tp = pFields[1];
		StringBuilder en = new StringBuilder();
		appendIfFilled(en, "Interj", pFields[2]);
		appendIfFilled(en, "Noun", pFields[3]);
		appendIfFilled(en, "Modifier", pFields[4]);
		appendIfFilled(en, "Verb", pFields[5]);
		appendIfFilled(en, "Verb", pFields[6]);
		appendIfFilled(en, "Conj", pFields[7]);
		appendIfFilled(en, "Sep", pFields[8]);
		appendIfFilled(en, "Prep", pFields[9]);
		appendIfFilled(en, "Cxt", pFields[10]);
		appendIfFilled(en, "Noun", pFields[11]);

		final String mem = pFields[13];
		String front;
		String back;
		if (pOrder == 0)
		{
			front = "Toki Pona: " + tp + ". In English?";
			back = en.toString();
		}
		else
		{
			front = "English: " + en.toString() + ". In Toki Pona?";
			back = tp;
		}
		return new AnkiCard[] {new AnkiCard(pNoteId, pNextLearn, pNbSuccess, pOrder, front, back, mem, true)};
	}

	private void appendIfFilled(StringBuilder pContainer, String pType, String pNew)
	{
		if ((pNew != null) && (pNew.trim().length() > 0))
		{
			if (pContainer.length() > 0)
				pContainer.append(", ");
			if ("/".equals(pType))
				pContainer.append("/").append(pNew);
			else
				pContainer.append(pType).append(": ").append(pNew);
		}
	}
}
