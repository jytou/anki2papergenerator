package jyt.anki.card.generator;

public class AnkiCard
{
	private long mNextLearn;// next millisecond when it should be learned, -1 if new
	private long mNbSuccess;// Number of last successes, thus giving the interval, -1 if new
	private long mOrder;// For new cards, it is important to sort cards in the correct order
	private String mFront;
	private String mBack;
	private String mMem;
	private long mNoteId;
	private boolean mDoubleSided;

	public AnkiCard(long pNoteId, long pNextLearn, long pNbSuccess, long pOrder, String pFront, String pBack, String pMem, boolean pDoubleSided)
	{
		super();
		mNoteId = pNoteId;
		mNextLearn = pNextLearn;
		mNbSuccess = pNbSuccess;
		mOrder = pOrder;
		mFront = pFront;
		mBack = pBack;
		mMem = pMem;
		mDoubleSided = pDoubleSided;
	}
	public long getNoteId()
	{
		return mNoteId;
	}
	public long getNextLearn()
	{
		return mNextLearn;
	}
	public long getNbSuccess()
	{
		return mNbSuccess;
	}
	public long getOrder()
	{
		return mOrder;
	}
	public String getFront()
	{
		return mFront;
	}
	public String getBack()
	{
		return mBack;
	}
	public String getMem()
	{
		return mMem;
	}
	public boolean isDoubleSided()
	{
		return mDoubleSided;
	}
}
